package com.trendgetter.config;

import com.trendgetter.batch.JobCompletionListener;
import com.trendgetter.batch.Processor;
import com.trendgetter.model.Category;
import com.trendgetter.model.CategoryDailyResult;
import com.trendgetter.service.Allegro;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

/**
 * Created by KKrzyworączka on 01.08.17.
 */

@Configuration
public class BatchConfig {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;
    @Autowired
    public StepBuilderFactory stepBuilderFactory;
    @Autowired
    private DatabaseConfig databaseConfig;
    @Value(value = "classpath:Categories.txt")
    private Resource categories;

    @Bean
    public Job processJob() throws Exception {
        return jobBuilderFactory.get("processJob").incrementer(new RunIdIncrementer()).listener(listener()).flow(orderStep1()).end().build();
    }

    @Bean
    public Step orderStep1() throws Exception {
        return stepBuilderFactory.get("orderStep1").<Category, CategoryDailyResult>chunk(100).reader(reader()).processor(new Processor()).writer(writer()).build();
    }

    @Bean
    public JobExecutionListener listener() {
        return new JobCompletionListener();
    }


    /*@Bean
    public ItemReader<Category> reader() {
        FlatFileItemReader<Category> reader = new FlatFileItemReader<>();
        reader.setResource(new ClassPathResource("Categories.txt"));
        reader.setLineMapper(lineMapper());
        return reader;
    }

    @Bean
    public LineMapper<Category> lineMapper() {
        DefaultLineMapper<Category> lineMapper = new DefaultLineMapper<>();

        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setDelimiter("#");

        BeanWrapperFieldSetMapper<Category> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(Category.class);

        lineMapper.setLineTokenizer(lineTokenizer);
        lineMapper.setFieldSetMapper(categoryFieldSetMapper());

        return lineMapper;
    }

    @Bean
    public CategoryFieldSetMapper categoryFieldSetMapper() {
        return new CategoryFieldSetMapper();
    }*/

    /*@Bean
    public ItemWriter<String> writer() {
        return new Writer();
    }*/

    @Bean
    public ItemProcessor<Category, CategoryDailyResult> processor() {
        return new Processor();
    }


    @Bean
    public ItemWriter<CategoryDailyResult> writer() {
        JpaItemWriter<CategoryDailyResult> writer = new JpaItemWriter<>();
        writer.setEntityManagerFactory(databaseConfig.entityManagerFactory().getObject());

        return writer;
    }


    @Bean(destroyMethod = "")
    public ItemReader<Category> reader() throws Exception {
        String query = "select c from Category c";

        JpaPagingItemReader<Category> reader = new JpaPagingItemReader<>();
        reader.setQueryString(query);
        reader.setEntityManagerFactory(databaseConfig.entityManagerFactory().getObject());
        reader.setPageSize(50);

        return reader;
    }
}

