package com.trendgetter.batch;

import com.allegro.trendgetter.webapi.DoGetItemsListResponse;
import com.allegro.trendgetter.webapi.ItemsListType;
import com.trendgetter.dao.CategoryDailyResultDao;
import com.trendgetter.model.Category;
import com.trendgetter.model.CategoryDailyResult;
import com.trendgetter.service.Allegro;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

/**
 * Created by KKrzyworączka on 01.08.17.
 */


public class Processor implements ItemProcessor<Category, CategoryDailyResult> {

    @Autowired
    private CategoryDailyResultDao dailyResultDao;

    @Autowired
    private Allegro allegro;

    @Override
    public CategoryDailyResult process(Category offer) throws Exception {
        CategoryDailyResult lastDailyResult = dailyResultDao.getLastDailyResultOfCategory(offer.getCategory_id());
        CategoryDailyResult currentDailyResult = createCurrentResult(offer.getCategory_id());

        currentDailyResult.setDealsMadeDiff(lastDailyResult.getDealsMade()-currentDailyResult.getDealsMade());
        currentDailyResult.setSoldPiecesDiff(lastDailyResult.getSoldPieces()-currentDailyResult.getSoldPieces());
        currentDailyResult.setSoldValueDiff(lastDailyResult.getSoldProductsValue().min(currentDailyResult.getSoldProductsValue()));

        return currentDailyResult;
    }

    private CategoryDailyResult createCurrentResult(Long categoryId){
        DoGetItemsListResponse response;
        CategoryDailyResult dailyResult = new CategoryDailyResult(categoryId);
        Float soldProductsValue = 0f;
        Long soldPieces = 0L;
        Long dealsMade = 0L;
        int offset = 0;
        int x = 0;
        do {
            response = allegro.doGetCategoryItemsList(categoryId.intValue(), offset);
            for (ItemsListType item : response.getItemsList().getItem()) {
                soldProductsValue += item.getBidsCount() * item.getPriceInfo().getItem().get(0).getPriceValue();
                soldPieces += item.getBidsCount();
                dealsMade += item.getBiddersCount();
            }
            offset += 1000;
            x++;
        } while (x <= (response.getItemsCount() / 1000));
        dailyResult.setSoldProductsValue(new BigDecimal(String.valueOf(soldProductsValue)));
        dailyResult.setSoldPieces(soldPieces);
        dailyResult.setDealsMade(dealsMade);
        dailyResult.finish();

        return dailyResult;
    }
}
