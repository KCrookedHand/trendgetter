package com.trendgetter.batch;

import org.springframework.batch.item.ItemWriter;

import java.util.List;

/**
 * Created by KKrzyworączka on 01.08.17.
 */
public class Writer implements ItemWriter<String> {

    private Integer i = 1;

    @Override
    public void write(List<? extends String> offers) throws Exception {
        for (String offer : offers) {
            System.out.println("Offer #" + i++ + ": " + offer);
        }
    }
}
