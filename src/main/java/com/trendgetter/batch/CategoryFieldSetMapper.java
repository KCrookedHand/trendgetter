package com.trendgetter.batch;

import com.trendgetter.model.Category;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

public class CategoryFieldSetMapper implements FieldSetMapper<Category> {

    @Override
    public Category mapFieldSet(FieldSet fieldSet) throws BindException {
        Category category = new Category();

        category.setCategory_id(fieldSet.readLong(0));
        category.setName(fieldSet.readString(1));
        category.setParent_id(fieldSet.readLong(2));

        return category;
    }
}
