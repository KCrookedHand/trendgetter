package com.trendgetter.controller;

import com.trendgetter.service.HomeService;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by kamil on 27.07.17.
 */

@RestController
public class HomeController {

    @Autowired
    public JobLauncher jobLauncher;
    @Autowired
    public Job processJob;
    public HomeService homeService;

    @Autowired
    public HomeController(HomeService homeService) {
        this.homeService = homeService;
    }

    private void log(Object message) {
        System.out.println(message);
    }

    @RequestMapping(value = "/user/{name}", method = RequestMethod.GET)
    public List<String> getUserAuctions(@PathVariable(value = "name") String name) {
        Integer userId = homeService.findUserId(name);
        List<String> userOffers = homeService.getUserOffers(userId);
        return userOffers;
    }

    @RequestMapping(value = "/batch")
    public String handleBatch() throws JobExecutionException {
        JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis()).toJobParameters();
        jobLauncher.run(processJob, jobParameters);
        return "Batch jobs invoked";
    }

    @RequestMapping(value = "/categories")
    public void getCategories() {
        homeService.getCategories();
    }

    @RequestMapping(value = "/categories/{categoryId}")
    public void categoryItems(@PathVariable(value = "categoryId") Long categoryId) {
        Long startTime = System.nanoTime();
        homeService.getCategoryOffers(categoryId);
        Long stopTime = System.nanoTime();
        System.out.println((stopTime - startTime) / 1000 + " sekund");
    }
}
