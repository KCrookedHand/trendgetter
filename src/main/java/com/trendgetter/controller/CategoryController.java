package com.trendgetter.controller;

import com.trendgetter.model.CategoryDailyResult;
import com.trendgetter.service.CategoryService;
import com.trendgetter.service.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CategoryController {

    private ICategoryService categoryService;

    @Autowired
    public CategoryController(ICategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @RequestMapping(value = "daily-result/{categoryId}", method = RequestMethod.GET)
    public ResponseEntity<CategoryDailyResult> getCategoryDailyResult(@PathVariable("categoryId") Long categoryId) {
        CategoryDailyResult result = categoryService.findCategoryDailyResult(categoryId);
        if (result == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
