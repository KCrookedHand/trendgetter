package com.trendgetter;

import com.trendgetter.service.Allegro;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;

import javax.xml.soap.SOAPException;

@SpringBootApplication
@EnableBatchProcessing
@EnableScheduling
@Configuration
public class TrendgetterApplication {

    public static void main(String[] args) {
        SpringApplication.run(TrendgetterApplication.class, args);
    }


    @Bean
    public Jaxb2Marshaller marshaller() throws Exception {
        final Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("com.allegro.trendgetter.webapi");
        return marshaller;
    }

    @Bean
    public Allegro allegroClient(Jaxb2Marshaller marshaller) throws SOAPException {
        final Allegro client = new Allegro(messageFactory());
        client.setDefaultUri("https://webapi.allegro.pl/service.php");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }

    @Bean
    public SaajSoapMessageFactory messageFactory() {
        return new SaajSoapMessageFactory();
    }
}
