package com.trendgetter.model;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by KKrzyworączka on 04.08.17.
 */

@Entity
public class Category {

    @Id
    private Long category_id;
    private String name;
    private Long parent_id;

    public Long getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Long category_id) {
        this.category_id = category_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParent_id() {
        return parent_id;
    }

    public void setParent_id(Long parent_id) {
        this.parent_id = parent_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Category)) return false;

        Category category = (Category) o;

        if (category_id != null ? !category_id.equals(category.category_id) : category.category_id != null)
            return false;
        if (name != null ? !name.equals(category.name) : category.name != null) return false;
        return parent_id != null ? parent_id.equals(category.parent_id) : category.parent_id == null;
    }

    @Override
    public int hashCode() {
        int result = category_id != null ? category_id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (parent_id != null ? parent_id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Category{" +
                "category_id=" + category_id +
                ", name='" + name + '\'' +
                ", parent_id=" + parent_id +
                '}';
    }
}