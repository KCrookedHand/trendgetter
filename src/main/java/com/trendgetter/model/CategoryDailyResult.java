package com.trendgetter.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;

/**
 * Created by KKrzyworączka on 03.08.17.
 */

@Entity
public class CategoryDailyResult {

    @Id
    @GeneratedValue
    private Long id;
    private Long categoryId;
    private LocalDateTime createdOn;
    private Long offersCount;

    private BigDecimal soldProductsValue;
    private Long soldPieces;
    private Long dealsMade;

    private BigDecimal soldValueDiff;
    private Long soldPiecesDiff;
    private Long dealsMadeDiff;

    private BigDecimal averageProductValue;
    private BigDecimal averageTransactionValue;

    public CategoryDailyResult(Long categoryId) {
        this.createdOn = LocalDateTime.now();
        this.categoryId = categoryId;
    }

    public void finish() {
        this.setAverageProductValue(soldProductsValue.divide(new BigDecimal(soldPieces), 2, RoundingMode.HALF_UP));
        this.setAverageTransactionValue(soldProductsValue.divide(new BigDecimal(dealsMade), 2, RoundingMode.HALF_UP));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDateTime createdOn) {
        this.createdOn = createdOn;
    }

    public Long getOffersCount() {
        return offersCount;
    }

    public void setOffersCount(Long offersCount) {
        this.offersCount = offersCount;
    }

    public BigDecimal getSoldProductsValue() {
        return soldProductsValue;
    }

    public void setSoldProductsValue(BigDecimal soldProductsValue) {
        this.soldProductsValue = soldProductsValue;
    }

    public Long getSoldPieces() {
        return soldPieces;
    }

    public void setSoldPieces(Long soldPieces) {
        this.soldPieces = soldPieces;
    }

    public Long getDealsMade() {
        return dealsMade;
    }

    public void setDealsMade(Long dealsMade) {
        this.dealsMade = dealsMade;
    }

    public BigDecimal getSoldValueDiff() {
        return soldValueDiff;
    }

    public void setSoldValueDiff(BigDecimal soldValueDiff) {
        this.soldValueDiff = soldValueDiff;
    }

    public Long getSoldPiecesDiff() {
        return soldPiecesDiff;
    }

    public void setSoldPiecesDiff(Long soldPiecesDiff) {
        this.soldPiecesDiff = soldPiecesDiff;
    }

    public Long getDealsMadeDiff() {
        return dealsMadeDiff;
    }

    public void setDealsMadeDiff(Long dealsMadeDiff) {
        this.dealsMadeDiff = dealsMadeDiff;
    }

    public BigDecimal getAverageProductValue() {
        return averageProductValue;
    }

    public void setAverageProductValue(BigDecimal averageProductValue) {
        this.averageProductValue = averageProductValue;
    }

    public BigDecimal getAverageTransactionValue() {
        return averageTransactionValue;
    }

    public void setAverageTransactionValue(BigDecimal averageTransactionValue) {
        this.averageTransactionValue = averageTransactionValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CategoryDailyResult that = (CategoryDailyResult) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (categoryId != null ? !categoryId.equals(that.categoryId) : that.categoryId != null) return false;
        if (createdOn != null ? !createdOn.equals(that.createdOn) : that.createdOn != null) return false;
        if (soldProductsValue != null ? !soldProductsValue.equals(that.soldProductsValue) : that.soldProductsValue != null)
            return false;
        if (soldPieces != null ? !soldPieces.equals(that.soldPieces) : that.soldPieces != null) return false;
        if (dealsMade != null ? !dealsMade.equals(that.dealsMade) : that.dealsMade != null) return false;
        if (soldValueDiff != null ? !soldValueDiff.equals(that.soldValueDiff) : that.soldValueDiff != null)
            return false;
        if (soldPiecesDiff != null ? !soldPiecesDiff.equals(that.soldPiecesDiff) : that.soldPiecesDiff != null)
            return false;
        if (dealsMadeDiff != null ? !dealsMadeDiff.equals(that.dealsMadeDiff) : that.dealsMadeDiff != null)
            return false;
        if (averageProductValue != null ? !averageProductValue.equals(that.averageProductValue) : that.averageProductValue != null)
            return false;
        return averageTransactionValue != null ? averageTransactionValue.equals(that.averageTransactionValue) : that.averageTransactionValue == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (categoryId != null ? categoryId.hashCode() : 0);
        result = 31 * result + (createdOn != null ? createdOn.hashCode() : 0);
        result = 31 * result + (soldProductsValue != null ? soldProductsValue.hashCode() : 0);
        result = 31 * result + (soldPieces != null ? soldPieces.hashCode() : 0);
        result = 31 * result + (dealsMade != null ? dealsMade.hashCode() : 0);
        result = 31 * result + (soldValueDiff != null ? soldValueDiff.hashCode() : 0);
        result = 31 * result + (soldPiecesDiff != null ? soldPiecesDiff.hashCode() : 0);
        result = 31 * result + (dealsMadeDiff != null ? dealsMadeDiff.hashCode() : 0);
        result = 31 * result + (averageProductValue != null ? averageProductValue.hashCode() : 0);
        result = 31 * result + (averageTransactionValue != null ? averageTransactionValue.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CategoryDailyResult{" +
                "id=" + id +
                ", categoryId=" + categoryId +
                ", createdOn=" + createdOn +
                ", soldProductsValue=" + soldProductsValue +
                ", soldPieces=" + soldPieces +
                ", dealsMade=" + dealsMade +
                ", soldValueDiff=" + soldValueDiff +
                ", soldPiecesDiff=" + soldPiecesDiff +
                ", dealsMadeDiff=" + dealsMadeDiff +
                ", averageProductValue=" + averageProductValue +
                ", averageTransactionValue=" + averageTransactionValue +
                '}';
    }
}
