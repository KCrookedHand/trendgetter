package com.trendgetter.service;

import com.trendgetter.dao.CategoryDailyResultDao;
import com.trendgetter.model.CategoryDailyResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryService implements ICategoryService {

    private CategoryDailyResultDao dailyResultDao;

    @Autowired
    public CategoryService(CategoryDailyResultDao dailyResultDao) {
        this.dailyResultDao = dailyResultDao;
    }

    @Override
    public CategoryDailyResult findCategoryDailyResult(Long categoryId) {
        return dailyResultDao.getLastDailyResultOfCategory(categoryId);
    }
}
