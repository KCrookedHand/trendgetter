package com.trendgetter.service;

import com.allegro.trendgetter.webapi.*;
import com.trendgetter.dao.CategoryDailyResultDao;
import com.trendgetter.model.CategoryDailyResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by KKrzyworączka on 30.07.17.
 */

@Service
public class HomeService {

    public CategoryDailyResultDao dailyResultDao;
    private Allegro allegro;

    @Autowired
    public HomeService(Allegro allegro, CategoryDailyResultDao dailyResultDao) {
        this.allegro = allegro;
        this.dailyResultDao = dailyResultDao;
    }

    public Integer findUserId(String name) {
        DoGetUserIDResponse response = allegro.doGetUserId(name);
        return response.getUserId();
    }

    public List<String> getUserOffers(Integer userId) {
        DoGetItemsListResponse response = allegro.doGetItemsList(userId);
        List<String> items = new ArrayList<>();
        response.getItemsList().getItem().forEach(item -> items.add(item.getItemTitle()));
        return items;

    }

    public void getCategories() {
        DoGetCatsDataResponse response = allegro.doGetCategories();
        for (CatInfoType cat : response.getCatsList().getItem()) {
            System.out.println(cat.getCatId() + "#" + cat.getCatName() + "#" + cat.getCatParent());
        }
    }


    public void getCategoryOffers(Long categoryId) {
        //todo to hardcore refactor
        DoGetItemsListResponse response;
        CategoryDailyResult dailyResult = new CategoryDailyResult(categoryId);
        dailyResult.setCategoryId(categoryId);
        Float soldProductsValue = 0f;
        Long soldPieces = 0L;
        Long dealsMade = 0L;
        int offset = 0;
        int counter = 1;
        int x = 0;
        do {
            System.out.println("xxx");
            response = allegro.doGetCategoryItemsList(categoryId.intValue(), offset);
            for (ItemsListType item : response.getItemsList().getItem()) {
                try {
                    System.out.println(counter++ + " || " + item.getItemId() + " | " + item.getItemTitle() + " | " + item.getLeftCount() + " | " + item.getBidsCount() + " | " + item.getSellerInfo().getUserLogin() + " | " + item.getPriceInfo().getItem().get(0).getPriceValue());
                    soldProductsValue += item.getBidsCount() * item.getPriceInfo().getItem().get(0).getPriceValue();
                    System.out.println(soldProductsValue);
                    soldPieces += item.getBidsCount();
                    dealsMade += item.getBiddersCount();
                } catch (NullPointerException ex){
                    System.out.println(counter + " | " + item.getItemId() + " | " + item.getItemTitle());
                }
            }
            offset += 1000;
            x++;
        } while (x <= (response.getItemsCount() / 1000));
        dailyResult.setSoldProductsValue(new BigDecimal(String.valueOf(soldProductsValue)));
        dailyResult.setSoldPieces(soldPieces);
        dailyResult.setDealsMade(dealsMade);
        dailyResult.finish();
        dailyResultDao.save(dailyResult);
    }
}
