package com.trendgetter.service;

import com.allegro.trendgetter.webapi.*;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;

/**
 * Created by kamil on 27.07.17.
 */

public class Allegro extends WebServiceGatewaySupport {

    public Allegro(SaajSoapMessageFactory messageFactory) {
        super(messageFactory);
    }

    public DoGetUserIDResponse doGetUserId(String name) {
        DoGetUserIDRequest request = new DoGetUserIDRequest();
        request.setWebapiKey("15984a8f");
        request.setCountryId(1);
        request.setUserLogin(name);
        request.setUserEmail("");

        return (DoGetUserIDResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public DoGetItemsListResponse doGetItemsList(Integer userId) {
        DoGetItemsListRequest request = new DoGetItemsListRequest();

        ArrayOfFilteroptionstype filters = new ArrayOfFilteroptionstype();
        FilterOptionsType nameFilter = new FilterOptionsType();
        nameFilter.setFilterId("userId");
        ArrayOfString ids = new ArrayOfString();
        ids.getItem().add(Integer.toString(userId));
        nameFilter.setFilterValueId(ids);
        filters.getItem().add(nameFilter);

        request.setWebapiKey("15984a8f");
        request.setCountryId(1);
        request.setFilterOptions(filters);
        request.setResultSize(30);

        return (DoGetItemsListResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }


    public DoGetCatsDataResponse doGetCategories() {
        DoGetCatsDataRequest request = new DoGetCatsDataRequest();

        request.setWebapiKey("15984a8f");
        request.setCountryId(1);

        return (DoGetCatsDataResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public DoGetItemsListResponse doGetCategoryItemsList(Integer categoryId, Integer offset) {
        DoGetItemsListRequest request = new DoGetItemsListRequest();

        ArrayOfFilteroptionstype filters = new ArrayOfFilteroptionstype();
        FilterOptionsType nameFilter = new FilterOptionsType();
        nameFilter.setFilterId("category");
        ArrayOfString ids = new ArrayOfString();
        ids.getItem().add(Integer.toString(categoryId));
        nameFilter.setFilterValueId(ids);
        filters.getItem().add(nameFilter);

        request.setWebapiKey("15984a8f");
        request.setResultSize(1000);
        request.setResultOffset(offset);
        request.setCountryId(1);
        request.setFilterOptions(filters);

        return (DoGetItemsListResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }
}
