package com.trendgetter.service;

import com.trendgetter.model.CategoryDailyResult;
import org.springframework.stereotype.Service;

public interface ICategoryService {

    CategoryDailyResult findCategoryDailyResult(Long categoryId);
}
