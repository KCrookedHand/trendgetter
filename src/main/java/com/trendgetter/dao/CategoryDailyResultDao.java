package com.trendgetter.dao;

import com.trendgetter.model.CategoryDailyResult;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface CategoryDailyResultDao extends CrudRepository<CategoryDailyResult, Long> {

    @Query("select cdr from CategoryDailyResult cdr where cdr.categoryId = :categoryId and cdr.createdOn =" +
            " (select max(cdr.createdOn) from CategoryDailyResult cdr where cdr.categoryId = :categoryId)")
    CategoryDailyResult getLastDailyResultOfCategory(@Param("categoryId") Long categoryId);
}
